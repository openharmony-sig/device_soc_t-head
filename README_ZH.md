# device_t-head

## 1.简介

device_t-head子目录包含单核CB2201单板相关代码及脚本。

## 2.环境搭建

   1. 编译工具链安装

      提示：用户也可以直接使用下列指令来使用默认环境中配置好的编译器，跳过该步骤。

      若要使用默认环境请先执行 '3.获取源码' ，然后在根目录下执行下列指令安装默认编译器。

         ```shell
         sh build/prebuilts_download.sh
         ```

      可选的编译器安装流程：

      a) 创建`csky_toolchain`文件夹并进入

         ```shell
         mkdir csky_toolchain && cd csky_toolchain
         ```

      b) 下载csky-elfabiv2-tools-x86_64-minilibc-20210423.tar.gz 到`csky_toolchain`文件夹并解压，下载地址：https://occ.t-head.cn/community/download?id=3885366095506644992

         ```shell
         wget https://occ-oss-prod.oss-cn-hangzhou.aliyuncs.com/resource/1356021/1619529111421/csky-elfabiv2-tools-x86_64-minilibc-20210423.tar.gz
         tar -xf csky-elfabiv2-tools-x86_64-minilibc-20210423.tar.gz
         ```

      c) 将csky-elfabiv2编译工具链加入环境变量(将user_toolchain_xxx_path修改为自己的安装路径)：

         ```shell
         vim ~/.bashrc
         export PATH=$PATH:user_toolchain_xxx_path/csky_toolchain/bin
         source ~/.bashrc
         ```

## 3.获取源码

   [代码获取](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/get-code/sourcecode-acquire.md)

​   提示: 可以使用 `repo` 命令来获取源码。

   下载好源码后通过下列指令下载开发板代码：

   ```shell
      repo init -u https://gitee.com/openharmony-sig/manifest.git -m devboard_t-head.xml --no-repo-verify
      repo sync -c
      repo forall -c 'git lfs pull'
   ```

## 4.源码构建

   1. 执行hb set命令并选择项目`t-head ---> cb2201_demo`。

      ```shell
      hb set
      ```

   2. 进入kernel/liteos_m中执行make menuconfig选择Platform -> Chip -> qemu_csky_smartl 和 Platform -> SoC Series Selection -> CH2201 Series，配置当前开发板。

      ```shell
      cd kernel/liteos_m/
      make menuconfig
      ```

   3. 执行hb clean && hb build命令构建产生 `OHOS_Image` 的可执行文件。

      ```shell
      hb clean && hb build
      ```

   4. 在构建完成之后，对应的可执行文件在主目录下：

      ```
      out/cb2201/cb2201_demo/
      ```

## 5.在开发板中运行镜像

   1. 下载安装烧录工具

      a) 登录平头哥开发者论坛：https://occ.t-head.cn/auth/occlogin?from=%2F

      b) 下载下列两个工具：
      CDK 工具链接：https://occ-oss-prod.oss-cn-hangzhou.aliyuncs.com/resource/1356021/1638844524004/cdk-windows-V2.10.5-20211203-0928.zip
      CDS 工具链接：https://occ-oss-prod.oss-cn-hangzhou.aliyuncs.com/resource/1356021/1638781490202/cds-windows-mingw-elf_tools-V5.2.8-20211203-1616.zip

      注：相关串口驱动和下载器驱动安装CDK工具时会提示安装，按提示安装即可。
      注：以管理员权限运行cds安装软件，详细安装步骤请参考软件内的readme，安装完成后需要重启软件。

   2. 使用烧录工具

      第一步：使用CDK工具下载裸机代码。

        打开CDK工具，新建CB2201-BareMetal项目：

      ![新建CB2201-BareMetal项目截图1](https://gitee.com/openharmony-sig/device_soc_t-head/raw/master/figures/新建CB2201-BareMetal项目截图1.png)

      ![新建CB2201-BareMetal项目截图2](https://gitee.com/openharmony-sig/device_soc_t-head/raw/master/figures/新建CB2201-BareMetal项目截图2.png)

        编译下载裸机代码：

      ![编译下载裸机代码截图1](https://gitee.com/openharmony-sig/device_soc_t-head/raw/master/figures/编译下载裸机代码截图1.png)

        烧录成功：

      ![裸机烧录成功截图1](https://gitee.com/openharmony-sig/device_soc_t-head/raw/master/figures/裸机烧录成功截图1.png)

        按下开发板RESET按键后串口工具显示输出：

      ![裸机运行截图1](https://gitee.com/openharmony-sig/device_soc_t-head/raw/master/figures/裸机运行截图1.png)

      第二步：使用CDS工具下载`OHOS_Image` 的可执行文件。

        打开CDS工具，选择Flash选项：

      ![使用CDS烧录OHOS_Image镜像截图1](https://gitee.com/openharmony-sig/device_soc_t-head/raw/master/figures/使用CDS烧录OHOS_Image镜像截图1.png)

        配置下载选项并烧录OHOS_Image.bin文件：
      
      ![使用CDS烧录OHOS_Image镜像截图2](https://gitee.com/openharmony-sig/device_soc_t-head/raw/master/figures/使用CDS烧录OHOS_Image镜像截图2.png)
      
        按下开发板RESET按键后串口工具显示输出：

      ![OpenHarmony系统运行截图1](https://gitee.com/openharmony-sig/device_soc_t-head/raw/master/figures/OpenHarmony系统运行截图1.png)

   注：CDK烧录只需要运行一次保证运行成功即可，后续修改代码只需要运行CDS烧录OHOS_Image.bin。

